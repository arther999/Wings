﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wings.Users.Dto
{
    public class SetPassWordInput : IInputDto, ICustomValidate
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public void AddValidationErrors(List<System.ComponentModel.DataAnnotations.ValidationResult> results)
        {
            if(OldPassword==NewPassword)
            {
                results.Add(new System.ComponentModel.DataAnnotations.ValidationResult("old passwd can`t equals to new passwd"));
            }
        }
    }
}
