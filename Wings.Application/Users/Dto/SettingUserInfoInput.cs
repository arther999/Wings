﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wings.Users.Dto
{
    public class SettingUserInfoInput : IInputDto//, ICustomValidate
    {
        public string DisPlayName { get; set; }
        public string Email { get; set; }
    }
}
