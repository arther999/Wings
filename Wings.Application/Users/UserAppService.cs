using System.Threading.Tasks;
using Abp.Authorization;
using Wings.Users.Dto;
using Abp.Application.Services.Dto;
using Microsoft.AspNet.Identity;
using System.Linq;
using Abp.AutoMapper;
using System.Collections.Generic;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;


using Abp.Domain.Uow;
using Wings.Authorization.Roles;
using Abp.Authorization.Users;

namespace Wings.Users
{
    [AbpAuthorize]
    public class UserAppService : WingsAppServiceBase, IUserAppService
    {
        //private readonly UserManager _userManager;
        //private readonly IPermissionManager _permissionManager;
        private readonly IRepository<User, long> userRepository;
        private readonly IRepository<Role, int> roleRepository;
        private readonly IRepository<UserRole, long> userroleRepository;

        //public UserAppService(IRepository<User, long> userRepository, IRepository<Role, int> roleRepository, IPermissionManager permissionManager)
        RoleManager RoleManager;
        public UserAppService(IRepository<User, long> _userRepository,   IRepository<Role, int> _roleRepository,IRepository<UserRole, long> _userroleRepository, RoleManager _RoleManager)
        {
            this.RoleManager = _RoleManager;
            //this.RoleManager = _RoleManager;
            userRepository = _userRepository;
            userroleRepository = _userroleRepository;
            roleRepository = _roleRepository;
            //_permissionManager = permissionManager;
            //_roleRepository = roleRepository;

        }

        //public async Task ProhibitPermission(ProhibitPermissionInput input)
        //{
        //    var user = await _userManager.GetUserByIdAsync(input.UserId);
        //    var permission = _permissionManager.GetPermission(input.PermissionName);

        //    await _userManager.ProhibitPermissionAsync(user, permission);
        //}

        ////Example for primitive method parameters.
        //public async Task RemoveFromUser(long userId, string userName)
        //{
        //    CheckErrors(await _userManager.RemoveFromUserAsync(userId, userName));
        //}

        public PagedResultOutput<UserDto> GetAllUsers(GetUserInput input)
        {

            //判断是否拥有角色
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var TotalCount = 0;

                var querys = UserManager.Users.WhereIf(!string.IsNullOrEmpty(input.UserName), u => u.UserName.Contains(input.UserName));
                TotalCount = querys.Count();
                var users = querys.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();
                return new PagedResultOutput<UserDto>()
                {
                    TotalCount = TotalCount,
                    Items = users.MapTo<List<UserDto>>()
                };
            }

        }
        public Abp.Application.Services.Dto.PagedResultOutput<UserDto> GetUsers(GetUserInput input)
        {
            //var test = ;
            //var test1 = _permissionManager.GetAllPermissions(false);
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                // var test2 = _permissionManager.GetAllPermissions(Abp.MultiTenancy.MultiTenancySides.Host);
                var querys = UserManager.Users.WhereIf(true, u => u.TenantId == input.TenantId).WhereIf(!string.IsNullOrEmpty(input.UserName), u => u.UserName.Contains(input.UserName));
                var TotalCount = querys.Count();
                var users = querys.OrderBy(input.Sorting).PageBy(input).ToList();
                return new Abp.Application.Services.Dto.PagedResultOutput<UserDto>()
                {
                    TotalCount = TotalCount,
                    Items = users.MapTo<List<UserDto>>()
                };
            }
        }

        public async Task<UserDto> GetUser(int UserId)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var user = await UserManager.GetUserByIdAsync(UserId);
                var dto = user.MapTo<UserDto>();
                //dto.RoleNames =new List<string>();
                dto.GrantRoleNames = UserManager.GetRoles(UserId).ToList();

                dto.AllRoleNames = RoleManager.Roles.WhereIf(true, r => r.TenantId == user.TenantId).Select(r => r.Name).ToList();
                return dto;
            }
        }

        public async Task SaveUser(UserDto input)
        {




            var user = new User();
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {

                CheckErrors(await UserManager.CheckTenantyDuplicateUsernameOrEmailAddressAsync(input.Id, input.TenantId, input.UserName, input.EmailAddress));
                var GrantRoleNames = input.GrantRoleNames;
                var grantRoleIds = roleRepository.GetAll().Where(r => r.TenantId == input.TenantId && GrantRoleNames.Contains(r.Name)).Select(r => r.Id).ToList();
                if (input.Id > 0)
                {

                    user = await UserManager.GetUserByIdAsync(input.Id);
                    user.Name = input.Name;
                    user.Surname = input.Surname;
                    user.TenantId = input.TenantId;
                    user.EmailAddress = input.EmailAddress;
                    user.IsActive = input.IsActive;
                    user.UserName = input.UserName;
                    user.IsEmailConfirmed = input.IsEmailConfirmed;
                    await userRepository.UpdateAsync(user);
                }
                else
                {
                    user.Name = input.Name;
                    user.Surname = input.Surname;
                    user.TenantId = input.TenantId;
                    user.EmailAddress = input.EmailAddress;
                    user.IsActive = input.IsActive;
                    user.UserName = input.UserName;
                    user.Password = new PasswordHasher().HashPassword(string.IsNullOrEmpty(input.Password) ? "111111" : input.Password);
                    var defaultRoles = RoleManager.Roles.Where(r => r.IsDefault == true && r.TenantId == input.TenantId).Select(r => r.Name);
                    input.GrantRoleNames = defaultRoles.ToList();
                    user.Id = await userRepository.InsertAndGetIdAsync(user);
                }
                await userroleRepository.DeleteAsync(ur => ur.UserId == input.Id);
                grantRoleIds.ForEach(r =>
                {
                    userroleRepository.InsertAsync(new UserRole(input.Id, r));
                });
                await CurrentUnitOfWork.SaveChangesAsync();
            }

        }

        public async Task DeleteUser(int UserId)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var user = (await UserManager.GetUserByIdAsync(UserId));
                CheckErrors(await UserManager.DeleteAsync(user));
                await this.CurrentUnitOfWork.SaveChangesAsync();
            }
        }


        public async Task UpdateUserPermissions(UpdateUserPermissionsInput input)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                //// 获取用户已经拥有权限
                //var permissions = (await GetUserPermissions(input.UserId)).ToList();
                //var grantingPermissions = new List<string>();
                //var prohibitPermissions = new List<string>();
                //input.GrantedPermissionNames.ForEach(p =>
                //{
                //    //过滤 新权限》用户已拥有权限 待添加授权
                //    if (!permissions.Contains(p))
                //    {
                //        grantingPermissions.Add(p);
                //    }
                //    //过滤 用户已拥有权限》新权限  待禁用权限
                //});
                //permissions.ForEach(p =>
                //{
                //    if (!input.GrantedPermissionNames.Contains(p))
                //    {
                //        prohibitPermissions.Add(p);
                //    }
                //});
                await UserManager.SetGrantedPermissionsAsync(await UserManager.GetUserByIdAsync(input.UserId), PermissionManager.GetAllPermissions().Where(p => input.GrantedPermissionNames.Contains(p.Name)));
                //throw new System.NotImplementedException();
            }
        }

        public async Task<IList<string>> GetUserPermissions(long UserId)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {

                return (await UserManager.GetGrantedPermissionsAsync(await UserManager.GetUserByIdAsync(UserId))).Select(p => p.Name).ToList();
            }
        }
        public async Task ReSetUserPermissions(long UserId)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                await UserManager.ResetAllPermissionsAsync(userRepository.Get(UserId));
            }
        }
    }
}