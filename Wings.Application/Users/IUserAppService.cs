using System.Threading.Tasks;
using Abp.Application.Services;
using Wings.Users.Dto;
using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Wings.Users
{
    public interface IUserAppService : IApplicationService
    {

        /// <summary>
        /// 获取所有角色
        /// 角色管理员拥有此访问权限
        /// </summary>
        /// <param name="input">输入参数</param>
        /// <returns></returns>
        PagedResultOutput<UserDto> GetUsers(GetUserInput input);
        /// <summary>
        /// 获取单个角色信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<UserDto> GetUser(int UserId);
        /// <summary>
        /// 保存单个角色
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        Task SaveUser(UserDto input);
        /// <summary>
        /// 删除单个角色
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Task DeleteUser(int UserId);
        //    Task ProhibitPermission(ProhibitPermissionInput input);

        //    Task RemoveFromUser(long userId, string roleName);
        PagedResultOutput<UserDto> GetAllUsers(GetUserInput input);

        /// <summary>
        /// 更新角色权限
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task UpdateUserPermissions(UpdateUserPermissionsInput input);
        /// <summary>
        /// 获取角色的权限信息
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        Task<IList<string>> GetUserPermissions(long UserId);
        /// <summary>
        /// 重置用户权限 还原到角色授权模式
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Task ReSetUserPermissions(long UserId);

    }
}