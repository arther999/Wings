﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wings.Auditlogs.Dto;
using Abp.Linq.Extensions;
using System.Linq.Dynamic;
using Abp.AutoMapper;
namespace Wings.Auditlogs
{
    [AbpAuthorize]
    public class AuditLogsAppService : WingsAppServiceBase, IAuditLogsAppService
    {
        private readonly IRepository<AuditLog, long> auditLogRepository;
        private readonly IRepository<Users.User, long> userRepository;
        private readonly IRepository<MultiTenancy.Tenant> tenantRepository;
        public AuditLogsAppService(IRepository<AuditLog, long> _auditLogRepository, IRepository<Users.User, long> _userRepository, IRepository<MultiTenancy.Tenant> _tenantRepository)
        {
            this.auditLogRepository = _auditLogRepository;
            this.userRepository = _userRepository;
            this.tenantRepository = _tenantRepository;
        }
        public PagedResultOutput<AuditLogDto> GetAuditLogs(GetAuditLogsInput input)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var querys = (from a in auditLogRepository.GetAll()
                              join t in TenantManager.Tenants on a.TenantId equals t.Id into at
                              join u in UserManager.Users on a.UserId equals u.Id into au

                              from lat in at.DefaultIfEmpty()
                              from lau in au.DefaultIfEmpty()
                              select new AuditLogDto()
                              {
                                  BrowserInfo = a.BrowserInfo,
                                  ClientIpAddress = a.ClientIpAddress,
                                  ClientName = a.ClientName,
                                  ExecutionTime = a.ExecutionTime,
                                  Exception = a.Exception,
                                  ExecutionDuration = a.ExecutionDuration,
                                  MethodName = a.MethodName,
                                  Parameters = a.Parameters
                                  ,
                                  ServiceName = a.ServiceName,
                                  TenantId = a.TenantId,
                                  TenantName = lat.Name,
                                  UserId = lau.Id,
                                  UserName = lau.UserName

                              }).WhereIf(input.TenantId.HasValue, r => r.TenantId == input.TenantId)
                   .WhereIf(!string.IsNullOrEmpty(input.Search), a =>
                       a.BrowserInfo.ToLower().Contains(input.Search.ToLower())
                       || a.ClientIpAddress.ToLower().Contains(input.Search.ToLower())
                    || a.ClientName.ToLower().Contains(input.Search.ToLower())
                    || a.Exception.ToLower().Contains(input.Search.ToLower())
                    || a.Parameters.ToLower().Contains(input.Search.ToLower())
                    || a.MethodName.ToLower().Contains(input.Search.ToLower())
                    || a.UserName.ToLower().Contains(input.Search.ToLower())
                    || a.TenantName.ToLower().Contains(input.Search.ToLower())
                       );//.WhereIf(!string.IsNullOrEmpty(input.UserName), u => u.UserId.Contains(input.UserName));
                var TotalCount = querys.Count();
                // var auditLog = querys.OrderBy(input.Sorting).PageBy(input);
                var items =
                   (querys).OrderBy(input.Sorting).PageBy(input).ToList();

                return new Abp.Application.Services.Dto.PagedResultOutput<AuditLogDto>()
                {
                    TotalCount = TotalCount,
                    Items = items
                };
            }
        }
    }
}
