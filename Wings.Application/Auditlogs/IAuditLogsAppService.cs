﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wings.Auditlogs.Dto;
using Wings.Roles.Dto;

namespace Wings.Auditlogs
{
    public interface IAuditLogsAppService : IApplicationService
    {
        /// <summary>
        /// 获取审计操作日志
        /// </summary>
        /// <param name="input">输入参数</param>
        /// <returns></returns>
        PagedResultOutput<AuditLogDto> GetAuditLogs(GetAuditLogsInput input);
    }
}
