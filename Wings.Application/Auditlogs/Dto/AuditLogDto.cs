﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wings.Auditlogs.Dto
{

    [AutoMapFrom(typeof(AuditLog))]
    public class AuditLogDto// : CreationAuditedEntityDto<long>
    {
        // 摘要: 
        //     Browser information if this method is called in a web request.
        [MaxLength(256)]
        public virtual string BrowserInfo { get; set; }
        //
        // 摘要: 
        //     IP address of the client.
        [MaxLength(64)]
        public virtual string ClientIpAddress { get; set; }
        //
        // 摘要: 
        //     Name (generally computer name) of the client.
        [MaxLength(128)]
        public virtual string ClientName { get; set; }
        //
        // 摘要: 
        //     Exception object, if an exception occured during execution of the method.
        [MaxLength(2000)]
        public virtual string Exception { get; set; }
        //
        // 摘要: 
        //     Total duration of the method call as milliseconds.
        public virtual int ExecutionDuration { get; set; }
        //
        // 摘要: 
        //     Start time of the method execution.
        public virtual DateTime ExecutionTime { get; set; }
        //
        // 摘要: 
        //     Executed method name.
        [MaxLength(256)]
        public virtual string MethodName { get; set; }
        //
        // 摘要: 
        //     Calling parameters.
        [MaxLength(1024)]
        public virtual string Parameters { get; set; }
        //
        // 摘要: 
        //     Service (class/interface) name.
        [MaxLength(256)]
        public virtual string ServiceName { get; set; }
        /// <summary>
        /// 租户名字
        /// </summary>
        public virtual string TenantName { get; set; }
        /// <summary>
        /// 用户名字
        /// </summary>
        public virtual string UserName { get; set; }
        //
        // 摘要: 
        //     TenantId.
        public virtual int? TenantId { get; set; }
        //
        // 摘要: 
        //     UserId.
        public virtual long? UserId { get; set; }
    }
}
