﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Wings.Roles.Dto;
using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Wings.Roles
{
    public interface IRoleAppService : IApplicationService
    {
        /// <summary>
        /// 获取所有角色
        /// 角色管理员拥有此访问权限
        /// </summary>
        /// <param name="input">输入参数</param>
        /// <returns></returns>
        PagedResultOutput<RoleDto> GetRoles(GetRoleInput input);
        /// <summary>
        /// 获取单个角色信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //[AbpAuthorize("AdminTenancyOwner")]
        Task<RoleDto> GetRole(int RoleId);
        /// <summary>
        /// 保存单个角色
        /// </summary>
        /// <param name="Role"></param>
        /// <returns></returns>
        Task SaveRole(RoleDto input);
        /// <summary>
        /// 删除单个角色
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        Task DeleteRole(int RoleId);
        /// <summary>
        /// 更新角色权限
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task UpdateRolePermissions(UpdateRolePermissionsInput input);
        /// <summary>
        /// 获取角色的权限信息
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        Task<List<string>> GetRolePermissions(int RoleId);
    }


}
