﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Wings.Authorization.Roles;
using Wings.Roles.Dto;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using Abp.AutoMapper;
using System.Collections.Generic;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Wings.MultiTenancy;
using Abp;

namespace Wings.Roles
{
    [AbpAuthorize]
    public class RoleAppService : WingsAppServiceBase, IRoleAppService
    {
        private readonly RoleManager _roleManager;
        //private readonly IPermissionManager _permissionManager;
        //private readonly IRepository<Role, int> _roleRepository;
        //private readonly IRepository<Tenant, int> _tenantRepository;
        //private readonly IUnitOfWorkManager _unitOfWorkManager;
        //public RoleAppService(RoleManager roleManager, IRepository<Role, int> roleRepository, IRepository<Tenant, int> tenantRepository, IPermissionManager permissionManager, IUnitOfWorkManager unitOfWorkManager)
        public RoleAppService(RoleManager roleManager)
        {
            _roleManager = roleManager;
            //_roleRepository = roleRepository;
            //_tenantRepository = tenantRepository;
            //_permissionManager = permissionManager;
            //_unitOfWorkManager = unitOfWorkManager;
        }
        //[AbpAuthorize("Update RolePermissions")]
        public async Task UpdateRolePermissions(UpdateRolePermissionsInput input)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var role = await _roleManager.GetRoleByIdAsync(input.RoleId);

                var grantedPermissions = PermissionManager
                    .GetAllPermissions()
                    .Where(p => input.GrantedPermissionNames.Contains(p.Name))
                    .ToList();

                await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);
            }
        }
        //[AbpAuthorize("View Roles")]
        public Abp.Application.Services.Dto.PagedResultOutput<RoleDto> GetRoles(GetRoleInput input)
        {
            //var test = ;
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var querys = _roleManager.Roles.WhereIf(true, r => r.TenantId == input.TenantId).WhereIf(!string.IsNullOrEmpty(input.RoleDisplayName), u => u.DisplayName.Contains(input.RoleDisplayName));
                var TotalCount = querys.Count();
                var users = querys.OrderBy(input.Sorting).PageBy(input).ToList();
                return new Abp.Application.Services.Dto.PagedResultOutput<RoleDto>()
                {
                    TotalCount = TotalCount,
                    Items = users.MapTo<List<RoleDto>>()
                };
            }
        }
        //[AbpAuthorize("View Roles")]
        public async Task<RoleDto> GetRole(int RoleId)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var role = await _roleManager.GetRoleByIdAsync(RoleId);
                return role.MapTo<RoleDto>();
            }
        }
        [UnitOfWork]
        public async Task SaveRole(RoleDto input)
        {
            var role = new Role();
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                CheckErrors(await _roleManager.CheckDuplicateRoleNameAsync(input.Id, input.Name, input.DisplayName));
                if (input.Id > 0)
                {
                    //if (!PermissionChecker.IsGranted("Edit Roles"))
                    //{
                    //    throw new AbpAuthorizationException("need Permission [Edit Roles]");
                    //}
                    role = await _roleManager.GetRoleByIdAsync(input.Id);
                    role.Name = input.Name;
                    role.DisplayName = input.DisplayName;
                    role.IsDefault = input.IsDefault;
                    role.IsStatic = input.IsStatic;
                    role.TenantId = input.TenantId;
                    CheckErrors(await _roleManager.UpdateAsync(role));
                }
                else
                {
                    //if (!PermissionChecker.IsGranted("Create Roles"))
                    //{
                    //    throw new AbpAuthorizationException("need Permission [Create Roles]");
                    //}
                    role.Name = input.Name;
                    role.DisplayName = input.DisplayName;
                    role.IsDefault = input.IsDefault;
                    role.IsStatic = input.IsStatic;
                    role.TenantId = input.TenantId;

                    CheckErrors(await _roleManager.CreateAsync(role));
                    //_roleRepository.InsertAndGetId(role);//new Role() { DisplayName = "sdfsd", Name = "sdfs", TenantId = input.TenantId });
                }
                if (input.IsDefault)
                {
                    var defaultRoles = _roleManager.Roles.Where(r => r.IsDefault == true && r.Id != input.Id && r.TenantId == input.TenantId).ToList();
                    foreach (var item in defaultRoles)
                    {
                        item.IsDefault = false;
                        CheckErrors(await _roleManager.UpdateAsync(item));
                    }
                }


                await CurrentUnitOfWork.SaveChangesAsync();
            }

        }
        //[AbpAuthorize("Delete Roles")]
        public async Task DeleteRole(int RoleId)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var role = (await _roleManager.GetRoleByIdAsync(RoleId));
                CheckErrors(await _roleManager.DeleteAsync(role));
                await this.CurrentUnitOfWork.SaveChangesAsync();
            }
        }

        //[AbpAuthorize("View Roles")]
        public async Task<List<string>> GetRolePermissions(int RoleId)
        {
            using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var permissions = (await _roleManager.GetGrantedPermissionsAsync(RoleId));
                return permissions.Select(p => p.Name).ToList();
            }
        }
    }
}