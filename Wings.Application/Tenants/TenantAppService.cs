﻿using System.Threading.Tasks;
using Abp.Auditing;
using Abp.Authorization;
using Abp.AutoMapper;
using Wings.MultiTenancy;
using System.Linq;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using Abp.Application.Services.Dto;
using Wings.Tenants.Dto;
using System.Collections.Generic;
using Abp;
using Abp.Domain.Uow;
using Abp.Domain.Repositories;
using Wings.Authorization.Roles;

namespace Wings.Tenants
{
    [AbpAuthorize]
    public class TenantAppService : WingsAppServiceBase, ITenantAppService
    {
        private readonly RoleManager roleManager;
        //private readonly IPermissionManager _permissionManager;
        //private readonly IUnitOfWorkManager _unitOfWorkManager;
        //private readonly IRepository<Tenant, int> _tenantRepository;
        //public TenantAppService(IRepository<Tenant> tenantRepository, IPermissionManager permissionManager, IUnitOfWorkManager unitOfWorkManager)
        public TenantAppService(RoleManager _roleManager)
        {
            //_tenantRepository = tenantRepository;
            //_permissionManager = permissionManager;
            //_unitOfWorkManager = unitOfWorkManager;
            this.roleManager = _roleManager;
        }


        //[AbpAuthorize("Administration.TenantManagement.SuperOwner")]
        public async Task<Abp.Application.Services.Dto.PagedResultOutput<Tenants.Dto.TenantDto>> GetTenants(Tenants.Dto.GetTenantInput input)
        {
            var querys = TenantManager.Tenants.WhereIf(!string.IsNullOrEmpty(input.TenantName), u => u.TenancyName.Contains(input.TenantName) || u.Name.Contains(input.TenantName));
            var TotalCount = querys.Count();
            var users = querys.OrderBy(input.Sorting).PageBy(input).ToList();

            var id = AbpSession.TenantId;
            return new PagedResultOutput<TenantDto>()
            {
                TotalCount = TotalCount,
                Items = users.MapTo<List<TenantDto>>()
            };
        }

        public async Task<Tenants.Dto.TenantDto> GetTenant(int? TenantId)
        {
            ////获取当前登录用户的Tenant
            //if (!PermissionChecker.IsGranted((long)AbpSession.UserId, "Administration.TenantManagement.SuperOwner") && AbpSession.TenantId != TenantId)
            //{
            //    throw new AbpAuthorizationException("You are not authorized to get this tenant info!");
            //}
            return (await TenantManager.GetByIdAsync(TenantId.Value)).MapTo<TenantDto>(); ;// _tenantRepository.Get(TenantId.Value).MapTo<TenantDto>();
        }
        //[AbpAuthorize("Administration.TenantManagement.SuperOwner")]
        [UnitOfWork]
        public async Task CreateTenant(Tenants.Dto.CreateTenantInput tenantDto)
        {
            //using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            //{
            var tenant = new Tenant() { Name = tenantDto.Name, TenancyName = tenantDto.TenancyName, IsActive = true };
            CheckErrors(await TenantManager.CreateAsync(tenant));
            //}

            //_tenantRepository.InsertAndGetId();
            //_unitOfWorkManager.Current.SaveChanges();
        }
        //[AbpAuthorize("Administration.TenantManagement.SuperOwner")]
        public async Task UpdateTenant(Tenants.Dto.TenantDto tenant)
        {


            //using (this.CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            //{
            var tenantEntity = await TenantManager.GetByIdAsync(tenant.Id);
            tenantEntity.Name = tenant.Name;
            tenantEntity.TenancyName = tenant.TenancyName;
            tenantEntity.IsActive = tenant.IsActive;
            CheckErrors(await TenantManager.UpdateAsync(tenantEntity));
            //}




            //await _tenantRepository.UpdateAsync(tenantEntity);
        }
        //[AbpAuthorize("Administration.TenantManagement.SuperOwner")]
        public async Task DeleteTenant(int TenantId)
        {
            var tenant = await TenantManager.GetByIdAsync(TenantId);
            CheckErrors(await TenantManager.DeleteAsync(tenant));
            //var tenantEntity = await _tenantRepository.GetAsync(TenantId);
            //tenantEntity.IsDeleted = true;
            //await _tenantRepository.UpdateAsync(tenantEntity);
        }
    }
}