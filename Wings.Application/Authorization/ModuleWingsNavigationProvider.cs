﻿using Abp.Application.Navigation;
using Abp.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wings.Application.Authorization
{ /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class WingsNavigationProvider : NavigationProvider
    {

        public override void SetNavigation(INavigationProviderContext context)
        {
            //var WingsNavigation = context.Manager.Menus.Add["Module.Wings"];
            var SystemManageMenus = new MenuItemDefinition(
                            "SystemManage",
                            new LocalizableString("SystemManage", WingsConsts.LocalizationSourceName)
                             , url: "#/",
                            icon: "fa fa-home"
                //                            , requiresAuthentication: true
                //, requiredPermissionName: "ModuleWingsManage"
                            );
            SystemManageMenus.AddItem(
                       new MenuItemDefinition(
                           "TenantManage",
                           new LocalizableString("TenantManage", WingsConsts.LocalizationSourceName),
                           url: "#/TenantManage",
                           icon: "fa fa-home"
                // , requiresAuthentication: true
                //, requiredPermissionName: "TenantManage"
                           )
                   ).AddItem(
                       new MenuItemDefinition(
                           "UserManage",
                           new LocalizableString("UserManage", WingsConsts.LocalizationSourceName),
                           url: "#/UserManage",
                           icon: "fa fa-home"
                // , requiresAuthentication: true
                //, requiredPermissionName: "UserManage"
                           )
                   ).AddItem(
                       new MenuItemDefinition(
                           "RoleManage",
                           new LocalizableString("RoleManage", WingsConsts.LocalizationSourceName),
                           url: "#/RoleManage",
                           icon: "fa fa-info"
                //, requiresAuthentication: true
                //, requiredPermissionName: "RoleManage"
                           )

                   )
            .AddItem(
                       new MenuItemDefinition(
                           "AuditLogsManage",
                           new LocalizableString("AuditLogsManage", WingsConsts.LocalizationSourceName),
                           url: "#/AuditLogsManage",
                           icon: "fa fa-info"
                //, requiresAuthentication: true
                //, requiredPermissionName: "RoleManage"
                           )

                   );
            SystemManageMenus.AddItem(
                        new MenuItemDefinition(
                            "Home",
                            new LocalizableString("HomePage", WingsConsts.LocalizationSourceName),
                            url: "#/",
                            icon: "fa fa-home"
                            )
                    ).AddItem(
                        new MenuItemDefinition(
                            "About",
                            new LocalizableString("About", WingsConsts.LocalizationSourceName),
                            url: "#/about",
                            icon: "fa fa-info"
                //,requiresAuthentication:true
                //,requiredPermissionName:"About"
                            )

                    );
            context.Manager.MainMenu.AddItem(SystemManageMenus);
            //context.Manager.
        }
    }
}
