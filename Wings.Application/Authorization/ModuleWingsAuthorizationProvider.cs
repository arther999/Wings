﻿using Abp.Authorization;
using Abp.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wings.Application.Authorization
{
    public class ModuleWingsAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            var ModuleWingsManage = context.CreatePermission("ModuleWingsManage", new FixedLocalizableString("ModuleWingsManage"));
            ModuleWingsManage.CreateChildPermission("Administration.TenantManagement.SuperOwner", new FixedLocalizableString("Administration.TenantManagement.SuperOwner"));

            var TenantManage = ModuleWingsManage.CreateChildPermission("TenantManage", new FixedLocalizableString("TenantManage"));

            TenantManage.CreateChildPermission("View Tenants", new FixedLocalizableString("View Tenants"));
            TenantManage.CreateChildPermission("Create Tenants", new FixedLocalizableString("Create Tenants"));
            TenantManage.CreateChildPermission("Edit Tenants", new FixedLocalizableString("Edit Tenants"));
            TenantManage.CreateChildPermission("Delete Tenants", new FixedLocalizableString("Delete Tenants"));

            TenantManage.CreateChildPermission("Update TenantSetting", new FixedLocalizableString("Update TenantSetting"));


            var UserManage = ModuleWingsManage.CreateChildPermission("UserManage", new FixedLocalizableString("UserManage"));

            UserManage.CreateChildPermission("View Users", new FixedLocalizableString("View Users"));
            UserManage.CreateChildPermission("Create Users", new FixedLocalizableString("Create Users"));
            UserManage.CreateChildPermission("Edit Users", new FixedLocalizableString("Edit Users"));
            UserManage.CreateChildPermission("Delete Users", new FixedLocalizableString("Delete Users"));
            UserManage.CreateChildPermission("Set UsersRole", new FixedLocalizableString("Set UsersRole"));

            UserManage.CreateChildPermission("Update UserPermissions", new FixedLocalizableString("Update UserPermissions"));

            var RoleManage = ModuleWingsManage.CreateChildPermission("RoleManage", new FixedLocalizableString("RoleManage"));

            RoleManage.CreateChildPermission("View Roles", new FixedLocalizableString("View Roles"));
            RoleManage.CreateChildPermission("Create Roles", new FixedLocalizableString("Create Roles"));
            RoleManage.CreateChildPermission("Edit Roles", new FixedLocalizableString("Edit Roles"));
            RoleManage.CreateChildPermission("Delete Roles", new FixedLocalizableString("Delete Roles"));

            RoleManage.CreateChildPermission("Update RolePermissions", new FixedLocalizableString("Update RolePermissions"));
        }
    }
}
