﻿using Abp.Configuration;
using Abp.Localization;
using Abp.Zero.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wings.Application.Configuration
{
    public class ModuleWingsSettingProvider : SettingProvider
    {
        public const string DefaultPageSize = "DefaultPageSize";

        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            SettingDefinitionGroup setttingGroup = new SettingDefinitionGroup("EmailSetting", new FixedLocalizableString("EmailSetting"));
            List<SettingDefinition> settings = new List<SettingDefinition>();
            settings.Add(new SettingDefinition(
                           AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin,
                           "false",
                           new FixedLocalizableString("Is email confirmation required for login."),
                           scopes: SettingScopes.Application | SettingScopes.Tenant
                           , group: setttingGroup
                           ));
            settings.Add(new SettingDefinition(
                           Abp.Net.Mail.EmailSettingNames.DefaultFromAddress,
                           string.Empty,
                           new FixedLocalizableString("Net.Mail.DefaultFromAddress"),
                           scopes: SettingScopes.Application | SettingScopes.Tenant
                           , group: setttingGroup
                           ));
            settings.Add(new SettingDefinition(
                          Abp.Net.Mail.EmailSettingNames.DefaultFromDisplayName,
                          string.Empty,
                          new FixedLocalizableString("Net.Mail.DefaultFromDisplayName"),
                          scopes: SettingScopes.Application | SettingScopes.Tenant
                          , group: setttingGroup
                          ));
            settings.Add(new SettingDefinition(
                         Abp.Net.Mail.EmailSettingNames.Smtp.Domain,
                         string.Empty,
                         new FixedLocalizableString("Net.Mail.Smtp.Domail"),
                         scopes: SettingScopes.Application | SettingScopes.Tenant
                         , group: setttingGroup
                         ));
            settings.Add(new SettingDefinition(
                        Abp.Net.Mail.EmailSettingNames.Smtp.EnableSsl,
                        string.Empty,
                        new FixedLocalizableString("Net.Mail.Smtp.EnableSsl"),
                        scopes: SettingScopes.Application | SettingScopes.Tenant
                        , group: setttingGroup
                        ));
            settings.Add(new SettingDefinition(
                        Abp.Net.Mail.EmailSettingNames.Smtp.Host,
                        string.Empty,
                        new FixedLocalizableString("Net.Mail.Smtp.Host"),
                        scopes: SettingScopes.Application | SettingScopes.Tenant
                        , group: setttingGroup
                        ));
            settings.Add(new SettingDefinition(
                      Abp.Net.Mail.EmailSettingNames.Smtp.Password,
                      string.Empty,
                      new FixedLocalizableString("Net.Mail.Smtp.Password"),
                      scopes: SettingScopes.Application | SettingScopes.Tenant
                      , group: setttingGroup
                      ));
            settings.Add(new SettingDefinition(
                    Abp.Net.Mail.EmailSettingNames.Smtp.Port,
                    string.Empty,
                    new FixedLocalizableString("Net.Mail.Smtp.Port"),
                    scopes: SettingScopes.Application | SettingScopes.Tenant
                    , group: setttingGroup
                    ));
            settings.Add(new SettingDefinition(
                  Abp.Net.Mail.EmailSettingNames.Smtp.UseDefaultCredentials,
                  string.Empty,
                  new FixedLocalizableString("Net.Mail.Smtp.UseDefaultCredentials"),
                  scopes: SettingScopes.Application | SettingScopes.Tenant
                  , group: setttingGroup
                  ));
            settings.Add(new SettingDefinition(
                Abp.Net.Mail.EmailSettingNames.Smtp.UserName,
                string.Empty,
                new FixedLocalizableString("Net.Mail.Smtp.UserName"),
                scopes: SettingScopes.Application | SettingScopes.Tenant
                , group: setttingGroup
                ));
            return settings;
        }
    }
}
