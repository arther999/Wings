﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;
using Abp.Localization.Sources.Xml;
using Wings.Application.Authorization;
using Wings.Application.Configuration;
using System.Web;

namespace Wings
{
    [DependsOn(typeof(WingsCoreModule), typeof(AbpAutoMapperModule))]
    public class WingsApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Add/remove localization sources here
            Configuration.Localization.Sources.Add(
                new XmlLocalizationSource(
                    WingsConsts.LocalizationSourceName,
                    //@"Z:\Projects\ABP\Wings\Wings.Web\App\Main\Localization"//
            HttpContext.Current.Server.MapPath("~/App/Main/Localization")
                    )
                );
            
            //Configure navigation/menu
            Configuration.Navigation.Providers.Add<WingsNavigationProvider>();
        }
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Configuration.Authorization.Providers.Add<ModuleWingsAuthorizationProvider>();
            
            Configuration.Settings.Providers.Add<ModuleWingsSettingProvider>();

            //AutoMapper.Mapper.CreateMap<Abp.Authorization.Permission, Wings.Permissions.Dto.PermissionDto>()
            //    .ForMember(dto => dto.DisplayName, opt => opt.MapFrom(e => e.DisplayName.Localize()))
            //    .ForMember(dto => dto.Description, opt => opt.MapFrom(e => e.Description.Localize()));
        }
    }
}
