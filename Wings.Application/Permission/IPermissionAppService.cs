﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wings.Permission
{
    /// <summary>
    /// 权限相关接口
    /// </summary>
    public interface IPermissionAppService : IApplicationService
    {
        /// <summary>
        /// 获取租户所有的权限
        /// </summary>
        /// <param name="TennatId">租户id 值为null时只获取本站点权限</param>
        void GetAllPermission(int? TennatId);
    }
}
