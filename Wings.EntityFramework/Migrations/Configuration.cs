using System.Data.Entity.Migrations;
using Wings.Migrations.SeedData;
using EntityFramework.DynamicFilters;

namespace Wings.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Wings.EntityFramework.WingsDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Wings";
        }

        protected override void Seed(Wings.EntityFramework.WingsDbContext context)
        {
            context.DisableAllFilters();
            new InitialDataBuilder(context).Build();
        }
    }
}
