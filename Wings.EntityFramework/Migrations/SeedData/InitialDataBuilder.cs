﻿using Wings.EntityFramework;

namespace Wings.Migrations.SeedData
{
    public class InitialDataBuilder
    {
        private readonly WingsDbContext _context;

        public InitialDataBuilder(WingsDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            new DefaultTenantRoleAndUserBuilder(_context).Build();
        }
    }
}
