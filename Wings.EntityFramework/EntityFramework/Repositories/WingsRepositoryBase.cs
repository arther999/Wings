﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace Wings.EntityFramework.Repositories
{
    public abstract class WingsRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<WingsDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected WingsRepositoryBase(IDbContextProvider<WingsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class WingsRepositoryBase<TEntity> : WingsRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected WingsRepositoryBase(IDbContextProvider<WingsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
