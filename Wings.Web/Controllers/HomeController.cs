﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace Wings.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : WingsControllerBase
    {
        public ActionResult Index()
        { 
            return View("~/App/Main/views/layouttest/layout.cshtml"); //Layout of the angular application.
        }
	}
}