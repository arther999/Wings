﻿using Abp.Web.Mvc.Controllers;

namespace Wings.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class WingsControllerBase : AbpController
    {
        protected WingsControllerBase()
        {
            LocalizationSourceName = WingsConsts.LocalizationSourceName;
        }
    }
}