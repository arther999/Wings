﻿//(function () {
//'use strict';

var app = angular.module('app', [
    'ngAnimate',
    'ngSanitize',
    'ui.router',
    'ui.bootstrap',
    'ui.jq',
    "oc.lazyLoad",
    'abp'
]);

app.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/',
        layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/'
    };

    $rootScope.settings = settings;

    return settings;
}]);
/* Setup App Main Controller */
app.controller('AppController', ['$scope', '$rootScope', 'appSession', function ($scope, $rootScope, $appSession) {
    $scope.session = $appSession;

    $scope.$on('$viewContentLoaded', function () {
        Metronic.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
    });
}]);
//Configuration for Angular UI routing.
app.config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/App/Main/views/home/home.cshtml',
                menu: 'Home' //Matches to name of 'Home' menu in WingsNavigationProvider
            })
            .state('about', {
                url: '/about',
                templateUrl: '/App/Main/views/about/about.cshtml',
                menu: 'About' //Matches to name of 'About' menu in WingsNavigationProvider
            })//;
              .state('TenantManage', {
                  url: '/TenantManage',
                  templateUrl: '/App/Main/views/TenantManage/TenantManage.cshtml',
                  menu: 'TenantManage'
                  ,
                  resolve: {
                      deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                          return $ocLazyLoad.load({
                              name: 'app',
                              insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                              files: [
                                  '/Theme/assets/global/plugins/select2/select2.css',
                                  '/Theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                                  '/Theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',

                                  '/Theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                  '/Theme/assets/global/plugins/select2/select2.min.js',
                                  '/Theme/assets/global/plugins/datatables/all.min.js',

                                  '/Theme/assets/global/scripts/datatable.js',


                              ]
                          });
                      }]
                  }
              })
            .state("TenantEdit", {
                url: '/TenantManage/Edit/:id',
                templateUrl: '/App/Main/views/TenantManage/TenantEdit.cshtml'
                ,resolve: {

                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'app',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                            files: [
                                '/Theme/assets/global/plugins/bootbox/bootbox.min.js',
                            ]
                        });
                    }]
                }
            })
        .state('UserMange', {
            url: '/UserManage',
            templateUrl: '/App/Main/views/UserManage/UserManage.cshtml',
            menu: 'UserManage',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '/Theme/assets/global/plugins/select2/select2.css',
                            '/Theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '/Theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',

                            '/Theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '/Theme/assets/global/plugins/select2/select2.min.js',
                            '/Theme/assets/global/plugins/datatables/all.min.js',

                            '/Theme/assets/global/scripts/datatable.js',
                            '/Theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css',
                            '/Theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css'

                        ]
                    });
                }]
            }
        }).state("UserEdit", {
            url: '/UserManage/Edit/:id',
            templateUrl: '/App/Main/views/UserManage/UserEdit.cshtml',
            resolve: {

                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '/Theme/assets/global/plugins/bootbox/bootbox.min.js',
                            '/Theme/assets/global/plugins/jstree/dist/themes/default/style.min.css',
                            '/Theme/assets/global/plugins/jstree/dist/jstree.min.js',

                        ]
                    });
                }]
            }
        }).state("profile", {
            url: '/Profile',
            templateUrl: '/App/Main/views/UserManage/UserProfile.cshtml' 
            ,resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'app',  
                    insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                    files: [
                        '/Theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                        '/Theme/assets/admin/pages/css/profile.css',
                        '/Theme/assets/admin/pages/css/tasks.css',
                            
                        '/Theme/assets/global/plugins/jquery.sparkline.min.js',
                        '/Theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

                        '/Theme/assets/admin/pages/scripts/profile.js',
                    ]                    
                });
            }]
            }
        }).state("profile.dashboard", {
            url: '/UserProfile/Dashboard',
            templateUrl: '/App/Main/views/UserManage/UserProfileDashboard.cshtml'
        }).state("profile.account", {
            url: '/UserProfile/Account',
            templateUrl: '/App/Main/views/UserManage/UserProfileAccount.cshtml'
        })
           .state('RoleMange', {
               url: '/RoleManage',
               templateUrl: '/App/Main/views/RoleManage/RoleManage.cshtml',
               menu: 'RoleManage',
               resolve: {
                   deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                       return $ocLazyLoad.load({
                           name: 'app',
                           insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                           files: [
                               '/Theme/assets/global/plugins/select2/select2.css',
                               '/Theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                               '/Theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',

                               '/Theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                               '/Theme/assets/global/plugins/select2/select2.min.js',
                               '/Theme/assets/global/plugins/datatables/all.min.js',

                               '/Theme/assets/global/scripts/datatable.js',
                               '/Theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css',
                               '/Theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css'

                           ]
                       });
                   }]
               }
           })
          .state("RoleEdit", {
              url: '/RoleManage/Edit/:id',
              templateUrl: '/App/Main/views/RoleManage/RoleEdit.cshtml',
              resolve: {

                  deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                      return $ocLazyLoad.load({
                          name: 'app',
                          insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                          files: [
                              '/Theme/assets/global/plugins/bootbox/bootbox.min.js',
                              '/Theme/assets/global/plugins/jstree/dist/themes/default/style.min.css',
                            '/Theme/assets/global/plugins/jstree/dist/jstree.js',

                          ]
                      });
                  }]
              }
          }).state('AuditLogsManage', {
              url: '/AuditLogsManage',
              templateUrl: '/App/Main/views/ViewAuditLogs/AuditLogsManage.cshtml',
              menu: 'AuditLogsManage'
                  ,
              resolve: {
                  deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                      return $ocLazyLoad.load({
                          name: 'app',
                          insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                          files: [
                              '/Theme/assets/global/plugins/select2/select2.css',
                              '/Theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                              '/Theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',

                              '/Theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                              '/Theme/assets/global/plugins/select2/select2.min.js',
                              '/Theme/assets/global/plugins/datatables/all.min.js',

                              '/Theme/assets/global/scripts/datatable.js',


                          ]
                      });
                  }]
              }
          });
    }
]);

app.run(["$rootScope", "settings", "$state", function ($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
}]);
// directives

/***
GLobal Directives
***/

// Route State Load Spinner(used on page or content load)
app.directive('ngSpinnerBar', ['$rootScope',
    function ($rootScope) {
        return {
            link: function (scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function () {
                    element.removeClass('hide'); // show spinner bar
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function () {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    //Layout.setSidebarMenuActiveLink('match'); // activate selected link in the sidebar menu

                    // auto scorll to page top
                    setTimeout(function () {
                        Metronic.scrollTop(); // scroll to the top on content load
                    }, $rootScope.settings.layout.pageAutoScrollOnLoad);
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function () {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function () {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
])

// Handle global LINK click
app.directive('a', function () {
    return {
        restrict: 'E',
        link: function (scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function (e) {
                    e.preventDefault(); // prevent link click for above criteria
                });
            }
        }
    };
});

// Handle Dropdown Hover Plugin Integration
app.directive('dropdownMenuHover', function () {
    return {
        link: function (scope, elem) {
            elem.dropdownHover();
        }
    };
});


//})();

// 对Date的扩展，将 Date 转化为指定格式的String 
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1,                 //月份 
        "d+": this.getDate(),                    //日 
        "h+": this.getHours(),                   //小时 
        "m+": this.getMinutes(),                 //分 
        "s+": this.getSeconds(),                 //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds()             //毫秒 
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}