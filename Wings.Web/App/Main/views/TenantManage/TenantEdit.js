﻿(function () {
    var controllerId = 'app.views.tenantedit';
    angular.module('app').controller(controllerId, [
        '$scope', '$stateParams', 'abp.services.app.tenant', function ($scope, $stateParams, $appService) {

            var vm = this;
            $scope.states = [{ id: true, text: '启用' }, { id: false, text: '停用' }];
            if ($stateParams.id) {
                $appService.getTenant($stateParams.id).success(function (result) {

                    $scope.tenantModel = result;
                });
                $scope.submit = function () {
                    $appService.updateTenant($scope.tenantModel).success(function (r) {
                        bootbox.confirm("修改成功！是否立刻返回？", function (result) {
                            if (result) {
                                history.back();
                            }
                        });


                    })
                }
                $scope.delete = function () {
                    bootbox.confirm("确定要删除此租户？删除之后租户将不能使用！", function (r) {
                        if (r) {
                            $appService.deleteTenant($scope.tenantModel.id).success(function (result) {
                                bootbox.alert("删除成功", function () {
                                    history.back();
                                });

                            })
                        }
                    })

                }
            } else {
                $scope.submit = function () {
                    $appService.createTenant($scope.tenantModel).success(function (result) {
                        bootbox.alert("添加成功！", function (result) {
                            history.back();
                        })

                    })
                }
            }

        }
    ]);
})();