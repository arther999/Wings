﻿(function () {
    var controllerId = 'app.views.tenantselect';
    angular.module('app').controller(controllerId, [
        '$scope', 'abp.services.app.tenant', function ($scope, $tenantService) {

            var vm = this;
            var tenant = $scope.session.tenant
            var vm = this;
            $scope.tenantId = tenant ? tenant.id : null;
            $scope.states = [{ id: true, text: '是' }, { id: false, text: '否' }];
            $tenantService.getTenants({ SkipCount: 0, MaxResultCount: 999 }).success(function (result) {
                $scope.tenants = [{ tenantName: "默认", name: "默认", id: null }].concat(result.items);
            })
        }
    ]);
})();