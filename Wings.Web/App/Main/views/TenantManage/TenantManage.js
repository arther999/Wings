﻿(function () {
    var controllerId = 'app.views.tenantmanage';
    angular.module('app').controller(controllerId, [
        '$scope', 'abp.services.app.tenant', function ($scope, $uerService) {

            //init date pickers
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                autoclose: true
            });
            //console.log($scope);
            var vm = this;
            //$scope.delete = function (id) {
            //    alert(id);
            //};
            vm.initDataTable = function () {
                //http://www.it165.net/pro/html/201407/18340.html
                $("#datatable_ajax").DataTable({
                    //"oLanguage": {//语言国际化
                    //    "sUrl": Metronic.getGlobalPluginsPath() + "datatables/media/js/jquery.dataTable.cn.txt"
                    //},
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条 ",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "第一页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 最后一页 "
                        }
                    },
                    columns: [
                        { data: "id" },
                        { data: 'name' },
                        { data: 'tenancyName' },
                        { data: 'creationTime' },
                        { data: 'isActive' },
                        { data: 'id' }
                    ],
                    "columnDefs": [
                         {
                             "targets": [3],
                             "data": "creationTime",
                             "render": function (data, type, full) {

                                 //'<a href="javascript:;" class="btn btn-xs default"><i class="fa fa-search"></i> View</a>',

                                 return new Date(data).Format("yyyy-MM-dd hh:mm:ss");
                             }
                         },
            {
                "targets": [4],
                "data": "isActive",
                "render": function (data, type, full) {

                    //'<a href="javascript:;" class="btn btn-xs default"><i class="fa fa-search"></i> View</a>',

                    return data ? '<span class="label label-sm label-success">启用</span>' : '<span class="label label-sm label-info">停用</span>';
                }
            }, {
                "targets": [5],
                "data": "id",
                "render": function (data, type, full) {
                    return '<a href="#/TenantManage/Edit/' + data + '" class="btn btn-xs default"><i class="fa fa-edit"></i> 管理</a>';// + '<a ng-click="delete(' + data + ')" class="btn btn-xs default"><i class="fa fa-warning"></i> 删除</a>';
                }
            }
                    ],
                    bServerSide: true,
                    "bSort": false,
                    fnServerData: function (sSource, aoData, fnCallback, oSettings) {
                        //var getaoData = function () {
                        //   for
                        //}
                        //var getorders = function (aodata) {
                        //    var orders = aodata[2].value;
                        //    var resultorders = [];
                        //    $.each(orders, function (i,v) {
                        //        resultorders.push(aodata[5].value[v.column] + " " + v.dir);
                        //    })
                        //    return resultorders.join(',');
                        //}
                        //console.log(getorders(aoData));
                        $uerService.getTenants({ SkipCount: oSettings._iDisplayStart, MaxResultCount: oSettings._iDisplayLength }).success(function (result) {
                            var data = {
                                "sEcho": 0,
                                "iTotalRecords": result.totalCount,
                                "iTotalDisplayRecords": result.totalCount,
                                "aaData": result.items
                            };
                            fnCallback(data)
                        })
                    }
                })

            }
        }
    ]);
})();