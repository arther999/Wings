﻿(function () {
    var controllerId = 'app.views.useredit';
    angular.module('app').controller(controllerId, [
        '$scope', '$stateParams', 'abp.services.app.user', 'abp.services.app.permission', 'abp.services.app.role', function ($scope, $stateParams, $appService, $permissionService, $roleService) {

            var vm = this;
            $scope.states = [{ id: true, text: '是' }, { id: false, text: '否' }];
            if ($stateParams.id) {
                $appService.getUser($stateParams.id).success(function (result) {
                    $scope.userModel = result;
                    var treedata = [];
                    $.each(result.allRoleNames, function (i, v) {
                        treedata.push({ text: v, id: v });
                    });

                    $('#tree_roles').bind("loaded.jstree", function (e, data) {
                        $.each(result.grantRoleNames, function (i, v) {
                            $('#tree_roles').jstree(true)
                          .select_node(v);
                        });

                    }).jstree({
                        'plugins': ["wholerow", "checkbox", "types"],
                        'core': {
                            "themes": {
                                "responsive": false
                            },
                            'data': treedata
                        },
                        "types": {
                            "default": {
                                "icon": "fa fa-folder icon-state-warning icon-lg"
                            },
                            "file": {
                                "icon": "fa fa-file icon-state-warning icon-lg"
                            }
                        }
                    });
                });
                $scope.submit = function () {
                    $scope.userModel.grantRoleNames = $('#tree_roles').jstree().get_selected()
                    $appService.saveUser($scope.userModel).success(function (r) {
                        bootbox.confirm("修改成功！是否立刻返回？", function (result) {
                            if (result) {
                                history.back();
                            } else {
                                vm.init();
                            }
                        });


                    })
                }
                $scope.delete = function () {
                    bootbox.confirm("确定要删除此角色？删除之后角色将不能使用！", function (r) {
                        if (r) {
                            $appService.deleteUser($scope.userModel.id).success(function (result) {
                                bootbox.alert("删除成功", function () {
                                    history.back();
                                });

                            })
                        }
                    })

                }

                //用户拥有权限管理
                vm.init = function () {

                    $("#tree_permissions").jstree("destroy");
                $permissionService.getPermissions().success(function (r) {
                    $('#tree_permissions').bind("loaded.jstree", function (e, data) {



                        $appService.getUserPermissions($stateParams.id).success(function (pResult) {
                            if (pResult) {
                                $.each(pResult, function (i, v) {
                                    $('#tree_permissions').jstree(true)
                                  .select_node(v);
                                });
                                data.inst.open_all(-1); // -1 opens all nodes in the container
                            }
                        })
                    }).jstree({
                        'plugins': ["wholerow", "checkbox", "types"],
                        'core': {
                            "themes": {
                                "responsive": false
                            },
                            'data': r
                        },
                        "types": {
                            "default": {
                                "icon": "fa fa-folder icon-state-warning icon-lg"
                            },
                            "file": {
                                "icon": "fa fa-file icon-state-warning icon-lg"
                            }
                        }
                    });



                })
                }
                // 保存权限
                $scope.saveUserPermissions = function () {
                    $appService.updateUserPermissions({ userId: $stateParams.id, grantedPermissionNames: $('#tree_permissions').jstree().get_selected() }).success(function (r) {
                        bootbox.alert("用户权限保存成功！", function () {
                            vm.init();
                        });
                    })
                }
                $scope.reSetUserPermissions = function () {
                    $appService.reSetUserPermissions($stateParams.id).success(function (r) {
                        bootbox.alert("重置权限成功！", function () {
                            vm.init();
                        });
                    })
                }
                vm.init();
            } else {
                $scope.submit = function () {
                    $scope.userModel.grantRoleNames = $('#tree_roles').jstree().get_selected()
                    $appService.createuser($scope.userModel).success(function (result) {
                        bootbox.alert("添加成功！", function (result) {
                            history.back();
                        })

                    })
                }
            }

        }
    ]);
})();