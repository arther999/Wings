﻿(function () {
    var controllerId = 'app.views.usermanage';
    angular.module('app').controller(controllerId, [
        '$scope', '$compile', 'abp.services.app.user', function ($scope, $compile, $userService) {
         
            var tenant = $scope.session.tenant
            var vm = this;
            $scope.tenantId = tenant?tenant.id:null;
            $scope.states = [{ id: true, text: '是' }, { id: false, text: '否' }];
            //$tenantService.getTenants({ SkipCount: 0, MaxResultCount: 999 }).success(function (result) {

            //    $scope.tenants = [{ tenantName: "默认", name: "默认", id: null }].concat(result.items);
            //}) 
            $scope.submit = function () {
                $scope.userModel.tenantId = $scope.tenantId;
                $userService.saveUser($scope.userModel).success(function (r) {
                    alert(r);
                    $scope.selectTenant($scope.tenantId);

                }) 
            }
            var table;

            $scope.selectTenant = function (tenantId) {
                $scope.tenantId = tenantId;
                if (table)
                { table.destroy(); }
                table = $("#datatable_ajax").DataTable({
                    "oLanguage": {//语言国际化
                        "sUrl": Metronic.getGlobalPluginsPath() + "datatables/media/js/jquery.dataTable.cn.txt"
                    },
                    columns: [
                        { data: "id" },
                        { data: 'name' },

                        { data: 'creationTime' },
                        { data: 'isDefault' },
                         { data: 'isStatic' },
                        { data: 'id' }
                    ],
                    "columnDefs": [
                         {
                             "targets": [2],
                             "data": "creationTime",
                             "render": function (data, type, full) {

                                 //'<a href="javascript:;" class="btn btn-xs default"><i class="fa fa-search"></i> View</a>',

                                 return new Date(data).Format("yyyy-MM-dd hh:mm:ss");
                             }
                         },
            {
                "targets": [3],
                "data": "isDefault",
                "render": function (data, type, full) {

                    //'<a href="javascript:;" class="btn btn-xs default"><i class="fa fa-search"></i> View</a>',

                    return data ? '<span class="label label-sm label-success">默认</span>' : '<span class="label label-sm label-info">自定义</span>';
                }
            }, {
                "targets": [4],
                "data": "isStatic",
                "render": function (data, type, full) {

                    //'<a href="javascript:;" class="btn btn-xs default"><i class="fa fa-search"></i> View</a>',

                    return data ? '<span class="label label-sm label-success">系统</span>' : '<span class="label label-sm label-info">用户</span>';
                }
            }, {
                "targets": [5],
                "data": "id",
                "render": function (data, type, full) {
                    return '<a  href="#/UserManage/Edit/' + data + '" class="btn btn-xs default"><i class="fa fa-edit"></i> 管理</a>';// + '<a ng-click="delete(' + data + ')" class="btn btn-xs default"><i class="fa fa-warning"></i> 删除</a>';
                }
            }
                    ],
                    bServerSide: true,
                    "bSort": false,
                    fnServerData: function (sSource, aoData, fnCallback, oSettings) {
                        $userService.getUsers({ TenantId: $scope.tenantId, SkipCount: oSettings._iDisplayStart, MaxResultCount: oSettings._iDisplayLength }).success(function (result) {

                            var data = {
                                "sEcho": 0,
                                "iTotalRecords": result.totalCount,
                                "iTotalDisplayRecords": result.totalCount,
                                "aaData": result.items
                            };
                            fnCallback(data)
                        })
                    }
                })

            }
        }
    ]);
})();