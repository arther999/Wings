﻿(function () {
    var controllerId = 'app.views.userprofile';
    angular.module('app').controller(controllerId, [
       '$rootScope', '$scope', '$http', '$timeout', '$stateParams', 'abp.services.app.user', function ($rootScope, $scope, $http, $timeout, $appService) {
            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax(); // initialize core components
                Layout.setMainMenuActiveLink('set', $('#sidebar_menu_link_profile')); // set profile link active in sidebar menu 
            });
        }
    ]);
})();