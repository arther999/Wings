﻿(function () {
    var controllerId = 'app.views.auditlogsmanage';
    angular.module('app').controller(controllerId, [
        '$scope', '$compile', 'abp.services.app.auditLogs', function ($scope, $compile, $auditLogsService) {
            var vm = this;

            var tenant = $scope.session.tenant
            var vm = this;
            $scope.tenantId = tenant ? tenant.id : null;

            var table;

            $scope.selectTenant = function (tenantId) {
                $scope.tenantId = tenantId;
                if (table)
                { table.destroy(); }
                table = $("#datatable_ajax").DataTable({
                    "oLanguage": {//语言国际化
                        "sUrl": Metronic.getGlobalPluginsPath() + "datatables/media/js/jquery.dataTable.cn.txt"
                    },
                    columns: [
                      { data: 'userName' },
                        { data: "tenantName" },
                        { data: 'executionTime' },
                        { data: 'serviceName' },
                        { data: 'methodName' },
                        { data: 'parameters' },
                        { data: 'executionDuration' },
                        { data: 'exception' },
                        { data: 'browserInfo' },
                        { data: 'clientName' },
                        { data: 'clientIpAddress' }
                    ],
                    "columnDefs": [
                         {
                             "targets": [2],
                             "data": "executionTime",
                             "render": function (data, type, full) {
                                 return new Date(data).Format("yyyy-MM-dd hh:mm:ss");
                             }
                         }
                    ],
                    bServerSide: true,
                    "bSort": false,
                    fnServerData: function (sSource, aoData, fnCallback, oSettings) {
                        console.log(oSettings.oPreviousSearch.sSearch);
                        
                        $auditLogsService.getAuditLogs({ Search:oSettings.oPreviousSearch.sSearch, TenantId: $scope.tenantId, SkipCount: oSettings._iDisplayStart, MaxResultCount: oSettings._iDisplayLength }).success(function (result) {

                            var data = {
                                "sEcho": 0,
                                "iTotalRecords": result.totalCount,
                                "iTotalDisplayRecords": result.totalCount,
                                "aaData": result.items
                            };
                            fnCallback(data)
                        })
                    }
                })

            }
        }
    ]);
})();