﻿(function () {
    var controllerId = 'app.views.roleedit';
    angular.module('app').controller(controllerId, [
        '$scope', '$stateParams', 'abp.services.app.role', 'abp.services.app.permission', function ($scope, $stateParams, $appService, $permissionService) {

            var vm = this;
            $scope.states = [{ id: true, text: '启用' }, { id: false, text: '停用' }];
            if ($stateParams.id) {
                $appService.getRole($stateParams.id).success(function (result) {
                    console.log(result);
                    $scope.roleModel = result;
                });
                $scope.submit = function () {

                    $appService.saveRole($scope.roleModel).success(function (r) {
                        bootbox.confirm("修改成功！是否立刻返回？", function (result) {
                            if (result) {
                                history.back();
                            }
                        });


                    })
                }
                $scope.delete = function () {
                    bootbox.confirm("确定要删除此角色？删除之后角色将不能使用！", function (r) {
                        if (r) {
                            $appService.deleteRole($scope.roleModel.id).success(function (result) {
                                bootbox.alert("删除成功", function () {
                                    history.back();
                                });

                            })
                        }
                    })

                }

                //角色权限管理

                $permissionService.getPermissions().success(function (r) {
                    $('#tree_permissions').bind("loaded.jstree", function (e, data) {



                        $appService.getRolePermissions($stateParams.id).success(function (pResult) {
                            if (pResult) {
                                $.each(pResult, function (i, v) {
                                    $('#tree_permissions').jstree(true)
                                  .select_node(v);
                                });
                                data.inst.open_all(-1); // -1 opens all nodes in the container
                            }
                        })
                    }).jstree({
                        'plugins': ["wholerow", "checkbox", "types"],
                        'core': {
                            "themes": {
                                "responsive": false
                            },
                            'data': r
                        },
                        "types": {
                            "default": {
                                "icon": "fa fa-folder icon-state-warning icon-lg"
                            },
                            "file": {
                                "icon": "fa fa-file icon-state-warning icon-lg"
                            }
                        }
                    });



                })
                // 保存权限
                $scope.saveRolePermissions = function () {
                    $appService.updateRolePermissions({ roleId: $stateParams.id, grantedPermissionNames: $('#tree_permissions').jstree().get_selected() }).success(function (r) {
                        bootbox.alert("角色权限保存成功！", function () {

                        });
                    })
                }
            } else {
                $scope.submit = function () {
                    $scope.roleModel.grantRoleNames = $('#tree_roles').jstree().get_selected()
                    $appService.createrole($scope.roleModel).success(function (result) {
                        bootbox.alert("添加成功！", function (result) {
                            history.back();
                        })

                    })
                }
            }

        }
    ]);
})();