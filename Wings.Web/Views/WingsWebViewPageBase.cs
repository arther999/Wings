﻿using Abp.Web.Mvc.Views;

namespace Wings.Web.Views
{
    public abstract class WingsWebViewPageBase : WingsWebViewPageBase<dynamic>
    {

    }

    public abstract class WingsWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected WingsWebViewPageBase()
        {
            LocalizationSourceName = WingsConsts.LocalizationSourceName;
        }
    }
}