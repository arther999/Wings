﻿using System.Reflection;
using Abp.Application.Services;
using Abp.Modules;
using Abp.WebApi;
using Abp.WebApi.Controllers.Dynamic.Builders;

namespace Wings
{
    [DependsOn(typeof(AbpWebApiModule), typeof(WingsApplicationModule))]
    public class WingsWebApiModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(WingsApplicationModule).Assembly, "app")
                .Build();
        }
    }
}
