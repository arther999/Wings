﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wings.Sessions;
using Wings.Tenants;
using Wings.Tenants.Dto;
using Wings.Tests.Sessions;
using Xunit;
using Shouldly;
using Wings.MultiTenancy;

namespace Wings.Tests.Tenants
{
    public class TenantAppService_Test : WingsTestBase
    {
        private readonly ITenantAppService _tenantAppService;

        public TenantAppService_Test()
        {
            _tenantAppService = Resolve<ITenantAppService>();

        }

        [Fact]
        public void GetTenants()
        {
            LoginAsHostAdmin();
            //var result = _tenantAppService.GetTenants(new GetTenantInput() { MaxResultCount = 10, SkipCount = 0 });
            //result.TotalCount.ShouldBeGreaterThan<int>(2);
        }
        [Fact]
        void GetTenant()
        {
            var result = _tenantAppService.GetTenant(1);
            
            var result2 = _tenantAppService.GetTenant(null);

        }
        [Fact]
        void CreateTenant()
        {
            _tenantAppService.CreateTenant(new CreateTenantInput() { Name = "11test", TenancyName = "11test" }).Wait();
        }
        [Fact]
        void UpdateTenant()
        {
            _tenantAppService.UpdateTenant(new TenantDto()
            {
                Id = 1,
                IsActive = true,
                TenancyName = "ddd",
                Name = "Default",

            });
        }
        void DeleteTenant()
        {

        }


    }
}
