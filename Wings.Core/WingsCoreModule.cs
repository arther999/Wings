﻿using System.Reflection;
using Abp.Modules;
using Abp.Zero;

namespace Wings
{
    [DependsOn(typeof(AbpZeroCoreModule))]
    public class WingsCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Remove the following line to disable multi-tenancy.
            Configuration.MultiTenancy.IsEnabled = true;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
