﻿using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using Abp.Threading;
using Abp.Localization;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Zero;

namespace Wings.Users
{
    public static class UserManagerExtensions
    {

        /// <summary>
        /// Check 租户内的用户名和邮箱是否重复了
        /// </summary>
        /// <param name="manager">User manager</param>
        /// <param name="userId">User id</param>
        /// <param name="permissionName">Permission name</param>
        public static async Task<IdentityResult> CheckTenantyDuplicateUsernameOrEmailAddressAsync<TTenant, TRole, TUser>(this AbpUserManager<TTenant, TRole, TUser> manager, long? expectedUserId, int? TenantId, string userName, string emailAddress)
            where TTenant : AbpTenant<TTenant, TUser>
            where TRole : AbpRole<TTenant, TUser>, new()
            where TUser : AbpUser<TTenant, TUser>
        {


            if (manager == null)
            {
                throw new ArgumentNullException("manager");
            }

            var L = new Func<string, string>(name => { return manager.LocalizationManager.GetString(AbpZeroConsts.LocalizationSourceName, name); });
            var user = (manager.Users.Where(u => u.TenantId == TenantId && u.UserName == userName).FirstOrDefault());
            if (user != null && user.Id != expectedUserId)
            {
                return AbpIdentityResult.Failed(string.Format(L("Identity.DuplicateName"), userName));
            }

            user = manager.Users.Where(u => u.TenantId == TenantId && u.EmailAddress == emailAddress).FirstOrDefault();
            if (user != null && user.Id != expectedUserId)
            {
                return AbpIdentityResult.Failed(string.Format(L("Identity.DuplicateEmail"), emailAddress));
            }

            return IdentityResult.Success;
        }
        //public static async Task<IdentityResult> SetTenantyRoles<TTenant, TRole, TUser>(this AbpUserManager<TTenant, TRole, TUser> manager, long UserId, string[] RoleNames)
        //    where TTenant : AbpTenant<TTenant, TUser>
        //    where TRole : AbpRole<TTenant, TUser>, new()
        //    where TUser : AbpUser<TTenant, TUser>
        //{
            
        //    if (manager == null)
        //    {
        //        throw new ArgumentNullException("manager");
        //    }
        //    var user = manager.Users.Where(u => u.Id == UserId).FirstOrDefault();
        //    user.Roles = null;
        //    var result = await manager.UpdateAsync(user);
        //    if (!result.Succeeded)
        //    {
        //        return AbpIdentityResult.Failed("无法充值角色");
        //    }
            
        //    result = await manager.AddToRolesAsync(UserId, RoleNames);
        //    if (!result.Succeeded)
        //    {
        //        return AbpIdentityResult.Failed("无法充值角色");
        //    }
        //    return IdentityResult.Success;
        //}
    }
}
