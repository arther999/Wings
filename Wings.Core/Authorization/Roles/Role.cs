﻿using Abp.Authorization.Roles;
using Wings.MultiTenancy;
using Wings.Users;

namespace Wings.Authorization.Roles
{
    public class Role : AbpRole<Tenant, User>
    {

    }
}