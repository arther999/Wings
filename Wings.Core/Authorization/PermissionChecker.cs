﻿using Abp.Authorization;
using Wings.Authorization.Roles;
using Wings.MultiTenancy;
using Wings.Users;

namespace Wings.Authorization
{
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
