﻿using Abp.Domain.Repositories;
using Abp.MultiTenancy;
using Wings.Authorization.Roles;
using Wings.Users;

namespace Wings.MultiTenancy
{
    public class TenantManager : AbpTenantManager<Tenant, Role, User>
    {
        public TenantManager(IRepository<Tenant> tenantRepository)
            : base(tenantRepository)
        {

        }
    }
}